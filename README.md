# Sensor Deployment File Repository #

This repository contains a set of Sensor Deployment Files (SDF) used in various projects. A SDF if a SensorML document used to model an instrument and its associated acquisition procedures. An SDF can be used with an interpreter software, i.e. the SWE Bridge, to automatically configure an acquisition process based on a sensor description.


**author**: Enoc Mart�nez, Daniel M. Toma         
**contact**: enoc.martinez@upc.edu, daniel.mihai.toma@upc.edu                           
**organization**: Universitat Polit�cnica de Catalunya (UPC) 